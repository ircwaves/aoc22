use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use std::num::ParseIntError;

fn most_calories(elves: &Vec<Vec<i64>>) -> Option<usize> {
    let index_of_max: Option<usize> = match elves.len() {
        0 => None,
        _ => elves
            .iter()
            .enumerate()
            .max_by(|(_, a), (_, b)| a.iter().sum::<i64>().cmp(&b.iter().sum::<i64>()))
            .map(|(index, _)| index)
    };
    return index_of_max;
}


fn main() -> Result<(), ParseIntError> {
    let args: Vec<String> = env::args().collect();
    // File hosts must exist in current path before this produces output
    let path = Path::new(&args[1]) ;
    let mut elves: Vec<Vec<i64>> = vec!() ;
    let mut elf: Vec<i64> = vec!();
    if let Ok(lines) = read_lines(path) {
        // Consumes the iterator, returns an (Optional) String
        for line in lines {
            if let Ok(ip) = line {
                match ip.len() {
                    0 => { // store counter value, and reset counter
                        elves.push(elf);
                        elf = vec!()
                    },
                    _ => { // parse as int and add to counter
                        let calories = match ip.parse::<i64>() {
                            Ok(num)  => num,
                            Err(e) => {
                                println!("Could not parse {}", ip);
                                return Err(e)

                            }
                        };
                        elf.push(calories);
                    }
                }
            }
        }
    }
    println!("which elf is best?");
    match most_calories(&elves) {
        Some(greedy_elf) => {
            println!("Index of max elf is {}.", greedy_elf);
            println!("And that elf has these fruit calories{:#?}.", elves[greedy_elf]);
            println!("Totalling up to {}.", elves[greedy_elf].iter().sum::<i64>());
        },
        None => {
            println!("musta been an empty list.");
        },
    };
    println!("Who are the top 3 elves?");
    elves.sort_by(|a,b| b.iter().sum::<i64>().cmp(&a.iter().sum::<i64>()));
    println!("{:#?}", elves[0]);
    println!("{:#?}", elves[1]);
    println!("{:#?}", elves[2]);
    println!("and those guys have {} total calories", elves[0].iter().sum::<i64>() + elves[1].iter().sum::<i64>() + elves[2].iter().sum::<i64>());
    Ok(())
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
